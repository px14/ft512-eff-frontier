efficient_frontier: main.cpp parse.hpp portfolio.hpp
	g++ -o efficient_frontier -g -std=gnu++98 -Wall -Werror -pedantic main.cpp
clean:
	rm -f efficient_frontier *~
