#include "eff_frontier.hpp"
#include "parse.hpp"
#include "portfolio.hpp"

#include <unistd.h>
#include <cstdlib>
#include <iostream>

void check_file_opened(ifstream& file, char* file_name){
  if (!file){
    cerr << "Error: unable to open file: " << file_name << endl;
    exit(EXIT_FAILURE);
  }
}


int main(int argc, char** argv){

  bool restricted = false;
  const char * err_msg = "Usage: efficient_frontier universe.csv correlation.csv [-r]";
  
  int opt;
  while((opt = getopt(argc, argv, "r")) != -1){
    switch(opt){
    case 'r':
      restricted = true;
      break;
    case '?':
      cerr << err_msg << endl;
      return EXIT_FAILURE;
    }	
  }
  
  if ((argc - optind) != 2){
    cerr << err_msg << endl;
    return EXIT_FAILURE;
  }
  
  // 1. open files
  ifstream universe_csv(argv[optind]);
  check_file_opened(universe_csv, argv[optind]);

  ifstream corr_csv(argv[optind + 1]);
  check_file_opened(corr_csv, argv[optind + 1]);

  try{
    // 2. import data  
    vector<Asset> universe = get_universe(universe_csv);
    size_t n_assets = universe.size();  
    MatrixXd correlation(n_assets, n_assets);
    fill_in_corr_matrix(correlation, corr_csv);

    // 3. build portfolio
    Portfolio portfolio(universe, correlation);
    portfolio.print_efficient_frontier(0.01, 0.26, restricted);
  }
  catch(unparsable_string& e){
    cerr << e.what() << endl; 
    return EXIT_FAILURE;
  }
  catch(missing_data& e){
    cerr << e.what() << endl; 
    return EXIT_FAILURE;
  }
  catch(unmatched_dimension& e){
    cerr << e.what() << endl;
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
}
