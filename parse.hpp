#ifndef PARSE_HPP
#define PARSE_HPP

#include "eff_frontier.hpp"
#include <fstream>
#include <cstring>
#include <string>


class unparsable_string: public exception {
public:
  virtual const char* what() const throw(){
    return "String can't be parsed to a number!\n";
  }
};


class missing_data: public exception {
public:
  virtual const char* what() const throw(){
    return "Missing data in universe file!\n";
  }
};

class unmatched_dimension: public exception {
public:
  virtual const char* what() const throw(){
    return "Correlation matrix has wrong dimension!\n";
  }
};


struct Asset {
  double ret; // annual return
  double vol; // volatility
  Asset(double r, double v): ret(r), vol(v){}
};


vector<string> split(string str, string delimeter){
  vector<string> values;
  string value;
  size_t dposition;
  while((dposition = str.find(delimeter)) != string::npos){
    value = str.substr(0, dposition);
    values.push_back(value);
    str.erase(0, dposition + delimeter.length());
  }
  values.push_back(str);
  
  return values;
}


double to_double(const char* str){
  if(str == NULL || !strcmp(str, "")){
    throw unparsable_string();
  }
  char* ptr;
  double value = strtod(str, &ptr); 
  if (strlen(ptr)){
    throw unparsable_string();
  }
  return value;
}


Asset line_to_asset(string line){
  vector<string> values = split(line, ",");

  if (values.size() < 3){
    throw missing_data();
  }
  
  double average_return = to_double(values[1].c_str());
  double volatility = to_double(values[2].c_str());
  return Asset(average_return, volatility);
}

  
vector<Asset> get_universe(ifstream& ufile){
  // ufile: a csv file for the asset universe 
  vector<Asset> assets;
  string curr;
  while(getline(ufile, curr)){
    Asset asset = line_to_asset(curr);
    assets.push_back(asset);
  }
  if (assets.size() == 0){
    throw missing_data();
  }
  return assets;
}


void fill_in_corr_matrix(MatrixXd& correlation, ifstream& cfile){
  // cfile: a csv file for the correlation matrix
  int r = 0;  // current row
  string curr;
  while(getline(cfile, curr)){
    // if the file has more rows than correlation, throw error
    if (r >= correlation.rows()){
      throw unmatched_dimension();
    }
    vector<string> values = split(curr, ",");
    // if the file doesn't have the correct number of cols, throw error
    if (values.size() != (size_t)correlation.cols()){
      throw unmatched_dimension();
    }
    // fill in current row
    for (int c = 0; c < correlation.cols(); c++){
      correlation(r, c) = to_double(values[c].c_str());
    }
    r++;
  }

  // if the file doesn't have enought rows, throw error 
  if (r < correlation.rows()){
    throw unmatched_dimension();
  }
}


#endif
