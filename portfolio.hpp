#ifndef PORTFOLIO_H
#define PORTFOLIO_H

#include "eff_frontier.hpp"

#include <cmath>
#include <cstdio>
#include <iostream>

class Portfolio{
  size_t n; // size of portfolio
  VectorXd rets;
  VectorXd vols;
  VectorXd weights;
  MatrixXd covariance;

  int has_negative_at(const VectorXd& v){
    for(int i = 0; i < v.size(); i++){
      if (v(i) < 0){
	      return i;
      }
    }
    return -1;
  }
  
  MatrixXd build_P(const MatrixXd& A){
    /* 
       generate Matrix from A:
       [sigma  A^T   
        A       0 ]    
    */

    size_t k = A.rows();
    MatrixXd P(n + k, n + k);
    P << covariance, A.transpose(), A, MatrixXd::Zero(k, k);
    return P;
  }

  VectorXd build_Q(const VectorXd& b){
    /*
      generate vector [0  b]^T
    */
    VectorXd Q(n + b.size());
    Q << VectorXd::Zero(n), b;
    return Q;
  }

  
  VectorXd optimize_under_constraint(const MatrixXd& A, const VectorXd& b){
    /*   
	 solve equation:
	 [sigma  A^T    [x          [0
	  A       0 ]    lambda]  =  b]
	  
	  call it P X = Q
      we can generate the new equation 
      from new constraint g(x) = Ax - b
    
    */
    MatrixXd P = build_P(A);
    VectorXd Q = build_Q(b);
    VectorXd X = P.fullPivLu().solve(Q);
    return X.head(n);
  }

  
public:
  Portfolio(const vector<Asset>& assets,
	    const MatrixXd& correlation):n(assets.size()),
					                         rets(n),
					                         vols(n),
					                         weights(n){
    for (size_t i = 0; i < n; i++){
      rets(i) = assets[i].ret;
      vols(i) = assets[i].vol;
    }
    MatrixXd vols_diag = vols.asDiagonal();
    covariance =  vols_diag * correlation * vols_diag;
  }
  
  double optimize(double rp, bool restricted){
    /*
      minimize 1/2* w^T * corr *w
      under constraint A*w = b
      
    rp: desired return of portfolio
    restricted: restrict weight to positive
    */

    
    /* 1. no restriction, w elements can be negative, so
       A = [1, 1, ..., 1       b = [1 
            r1, r2,...,rn ],        rp]
    */
    MatrixXd A(2, n);
    A.row(0) = VectorXd::Ones(n);
    A.row(1) = rets.transpose();
    VectorXd b(2);
    b << 1, rp;
    weights = optimize_under_constraint(A, b); 

    /* 2. with restriction
       If asset i is shortsold, add a restriction that asset i weighs 0.
       Optimize again until there is no negative elements in w.  */
    if (restricted){
      int i; // which asset is shortsold
      while((i = has_negative_at(weights)) >= 0){

	      // add a row [0, 0, ..., 1(ith element), ..., 0] to A
          VectorXd new_row = VectorXd::Zero(n).transpose();
          new_row(i) = 1;

          A.conservativeResize(A.rows() + 1, A.cols());
          A.row(A.rows() - 1) = new_row;
          
          // add an element 0 to b
          b.conservativeResize(b.size() + 1);
          b(b.size() - 1) = 0;

          // optimize again
          weights = optimize_under_constraint(A, b);
      }
    }    
    return sqrt(weights.transpose() * covariance * weights) ;
  }

  void print_efficient_frontier(double min_rp, double max_rp, bool restricted){
    printf("ROR,volatility\n");
    for (double rp = min_rp; rp < max_rp + 0.01; rp += 0.01){
      double volatility = optimize(rp, restricted);
      printf("%.1f%%,%.2f%%\n", rp * 100, volatility * 100);
    }
  }
};


#endif
